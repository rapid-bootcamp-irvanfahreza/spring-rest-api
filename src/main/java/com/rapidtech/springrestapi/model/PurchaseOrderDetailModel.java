package com.rapidtech.springrestapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseOrderDetailModel {
    private Long id;
    private Long poId;
    private Long productId;
    private Double quantity;
    private Double price;
}
