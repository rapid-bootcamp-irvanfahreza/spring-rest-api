package com.rapidtech.springrestapi.controller;

import com.rapidtech.springrestapi.model.EmployeeModel;
import com.rapidtech.springrestapi.model.ProductModel;
import com.rapidtech.springrestapi.model.PurchaseOrderModel;
import com.rapidtech.springrestapi.model.ResponseModel;
import com.rapidtech.springrestapi.service.PurchaseOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/po")
public class PoController {

    private PurchaseOrderService poService;

    @Autowired
    public PoController(PurchaseOrderService poService) {
        this.poService = poService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> savePo(@RequestBody PurchaseOrderModel request){
        // Optional<PurchaseOrderModel> result = poService.save(request);
        return ResponseEntity.ok().body(
                new ResponseModel(200,"SUCCESS", poService.save(request))
        );
    }
}
